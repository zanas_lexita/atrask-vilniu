# Atrask Vilniu

Klausimynas (TV Bokštas - III Lygis)
https://atrask-vilniu.firebaseapp.com/vieta;placeId=3xPQYFd72ChRhkzr6UpO

Kamera (TV Bokštas - III Lygis)
https://atrask-vilniu.firebaseapp.com/kamera;placeId=3xPQYFd72ChRhkzr6UpO

# Atrask Vilnių

## Firebase NoSQL Duombazės schema

```typescript
{
  levels: {                               // # Programėlės lygiai
    $id: {                                // # LYGIO ĮRAŠAS, $id - lygio ID
      scoreRequirement: number;           // Lygio reikalavimas (atrastų vietų skaičius)

      places: {                           // # Lygio vietos
        $id: {                            // # VIETOS ĮRAŠAS, $id - vietos ID
          coordinates: {                  // Vietos koordinatės
            latitude: number;             // Koordinatės platuma
            longitude: number;            // Koordinatės ilguma
          }
          description: string;            // Vietos aprašymas
          name       : string;            // Vietos pavadinimas
          photoURL   : string;            // Vietos nuotraukos nuoroda
          placeId    : string;            // Vietos ID

          questions: {                    // # Vietos klausimai
            $id: {                        // # KLAUSIMO ĮRAŠAS, $id - klausimo ID
              answers      : string[];    // Klausimo atsakymo variantai
              correctAnswer: number;      // Teisingo varianto numeris
              question     : string;      // Klausimas
              questionId   : string;      // Klausimo ID
            },
            ...                           // ... Kiti klausimai
          }
        },
        ...                               // ... Kitos vietos
      },
    },
    ...                                   // ... Kiti lygiai
  },
  users: {                                // # Programėlės vartotojų sąrašas
    $id: {                                // # VARTOTOJO ĮRAŠAS - $id - vartotojo ID
      displayName      : string;          // Vardas, pavardė
      email            : string;          // El. paštas
      level            : number;          // Pasiektas lygis
      photoURL         : string;          // Nuotraukos nuoroda
      placesVisited    : string[];        // Aplankytų vietų sąrašas, kuriame saugojami aplankytų vietų ID
      questionsAnswered: number;          // Teisingai atsakytų klausimų skaičius
      uid              : string;          // Vartotojo ID
    },
    ...                                   // ... Kiti vartotojai
  }
}
```
