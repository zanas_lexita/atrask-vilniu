import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

const updateLevels = level => {
  const levelId = Number(level);
  if (isNaN(levelId)) {
    console.error('Level ID is not a number.');
  }

  const prevLevel = levelId - 1;
  const nextLevel = levelId + 1;
  if (prevLevel === 0) {
    return admin
      .firestore()
      .doc(`levels/${levelId}`)
      .update({
        scoreRequirement: 0,
      })
      .catch(err => {
        console.error(
          'Could not update current level. `scoreRequirement = 0`',
          err
        );
      });
  } else {
    const currLevelDoc = admin.firestore().doc(`levels/${levelId.toString()}`);
    const prevLevelDoc = admin
      .firestore()
      .doc(`levels/${prevLevel.toString()}`);
    const nextLevelDoc = admin
      .firestore()
      .doc(`levels/${nextLevel.toString()}`);

    return prevLevelDoc
      .get()
      .then(prevLevelSnap => {
        if (prevLevelSnap.exists) {
          return prevLevelSnap.data().scoreRequirement || 0;
        }
        return 0;
      })
      .then(prevRequiredScore => {
        // Update current level based on previous
        return prevLevelDoc
          .collection('places')
          .get()
          .then(places => {
            const currSize = prevRequiredScore + places.size;

            return currLevelDoc
              .update({
                scoreRequirement: currSize,
              })
              .catch(err => {
                console.error('Could not update current level', err);
              });
          })
          .catch(err => {
            console.error('Could not get prev places', err);
          });
      })
      .then(() => {
        // Update next level based on current
        return currLevelDoc
          .get()
          .then(updatedCurrLevelSnapshot => {
            if (updatedCurrLevelSnapshot.exists) {
              const curLevelRequirement = updatedCurrLevelSnapshot.data()
                .scoreRequirement;
              return currLevelDoc
                .collection('places')
                .get()
                .then(curLevelPlacesDoc => {
                  return nextLevelDoc
                    .update({
                      scoreRequirement:
                        curLevelPlacesDoc.size + curLevelRequirement,
                    })
                    .catch(err => {
                      console.error(
                        "Error updating next level. Probably doesn't exist",
                        err
                      );
                    });
                })
                .catch(err => {
                  console.error('Error getting `curLevelPlacesDoc`', err);
                });
            }
            return Promise.reject("updatedCurrLevelSnapshot doesn't exist");
          })
          .catch(err => {
            console.error('Error getting `updatedCurrLevelSnapshot`', err);
          });
      })
      .catch(err => {
        console.error('Error happened', err);
      });
  }
};

export const updateLevelRequirementOnLevelChange = functions.firestore
  .document('levels/{levelId}')
  .onWrite((_, context) => {
    return updateLevels(context.params.levelId);
  });

export const updateLevelRequirementOnPlaceChange = functions.firestore
  .document('levels/{levelId}/places/{placeId}')
  .onWrite((_, context) => {
    return updateLevels(context.params.levelId);
  });
