import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

import * as levels from './levels';

admin.initializeApp(functions.config().firebase);

export const updateLevelRequirementOnLevelChange =
  levels.updateLevelRequirementOnLevelChange;
export const updateLevelRequirementOnPlaceChange =
  levels.updateLevelRequirementOnPlaceChange;
