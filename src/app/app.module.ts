import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment.prod';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Core Module (Auth, Database, etc...)
import { CoreModule } from './core/core.module';
// Material Design Components
import { SharedModule } from './shared/shared.module';

@NgModule({
  bootstrap: [AppComponent],
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule, // Routing
    SharedModule, // Material components, pages, custom components
    AngularFireModule.initializeApp(environment.firebase), // firebase/app,
    CoreModule,
  ],
})
export class AppModule {}
