import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { IUser } from '../../typings';
import { DataService } from './data.service';

@Injectable()
export class NewLevelGuard implements CanActivate {
  constructor(
    private data: DataService,
    private afs: AngularFirestore,
    private router: Router
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return combineLatest(this.data.levelData, this.data.user)
      .take(1)
      .switchMap(combined => {
        const remaining = combined[0].nextRemaining;
        const user = combined[1];

        if (remaining === 0) {
          return Observable.fromPromise(
            this.afs
              .collection('users')
              .doc<IUser>(user.uid)
              .update({
                level: ++user.level,
              })
          ).map(() => true);
        } else {
          // return Observable.of(true); // debug

          this.router.navigate(['/zemelapis']);

          return Observable.of(false);
        }
      });
  }
}
