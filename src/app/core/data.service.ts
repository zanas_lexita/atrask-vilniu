import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import {
  ILevel,
  ILevelData,
  ILevelItem,
  IPlace,
  IQuestion,
  IUser,
  IUserData,
} from '../../typings';
import { AuthPageComponent } from '../pages/auth-page/auth-page.component';
import { AuthService } from './auth.service';
import { combineLatest } from 'rxjs/observable/combineLatest';

@Injectable()
export class DataService {
  // Prisijungusio vartotojo informacija
  user: Observable<IUser>;
  userData: Observable<IUserData>;
  // Dabartinis lygis
  levelData: Observable<ILevelData>;
  nextLevel: Observable<number>;
  progress: Observable<number>;
  // Dabartinio lygio vietos
  places: Observable<IPlace[]>; // visos
  undiscoveredPlaces: Observable<IPlace[]>; // neatrastos
  discoveredPlaces: Observable<IPlace[]>; // atrastos

  constructor(
    private auth: AuthService,
    private afs: AngularFirestore,
    private dialog: MatDialog
  ) {
    // Get visited places for current user
    const placesUserVisited = this.auth.user.map(user => {
      return (user && user.placesVisited) || [];
    });

    this.user = auth.user;

    // Get current level of the user
    this.levelData = auth.user.switchMap(user => {
      const level = (user && user.level) || 1;
      const curLevelData = this.afs
        .collection('levels')
        .doc<ILevel>(level.toString())
        .valueChanges();
      const nextLevelDocRef = this.afs
        .collection('levels')
        .doc<ILevel>((level + 1).toString());

      const nextLevelData = nextLevelDocRef.valueChanges().switchMap(l => {
        if (l === null) return Observable.of(null);

        return Observable.of(l);
      });

      return nextLevelData.switchMap(nl => {
        return curLevelData.switchMap(cl => {
          // next level score (score that next level needs)
          const nls = nl ? nl.scoreRequirement : null;
          const next = nl ? user.level + 1 : null;
          // remaning score to next level
          const remaining = nls ? nls - user.placesVisited.length : null;

          const data: ILevelData = {
            current: user.level,
            currentRequired: cl.scoreRequirement,
            next,
            nextRemaining: remaining,
            nextRequired: nls,
          };

          return Observable.of(data);
        });
      });
    });

    this.progress = auth.user.switchMap(user => {
      return this.levelData.switchMap(ld => {
        if (ld.nextRequired === null) return Observable.of(100);

        const start = ld.currentRequired;
        const end = ld.nextRequired;
        const value = user.placesVisited.length;

        if (value > end) return Observable.of(100);
        if (value === 0 && end === 0) return Observable.of(0);

        const progress = (value - start) * 100 / (end - start);

        return Observable.of(progress);
      });
    });

    this.userData = auth.user.switchMap(user => {
      return this.levelData.switchMap(levelData => {
        const data: IUserData = {
          ...user,
          levelData,
          score: user.placesVisited.length,
        };

        return Observable.of(data);
      });
    });

    // Get all places for current level AND extend place with property if it's visited or not
    this.places = auth.user.switchMap(user => {
      const level = (user && user.level) || 1;

      // Get all places for user's current level
      const currentLevelDocRef = this.afs
        .collection('levels')
        .doc<ILevel>(level.toString());

      const currentLevelPlacesRef = currentLevelDocRef.collection<IPlace>(
        'places'
      );

      return currentLevelPlacesRef
        .valueChanges()
        .map(places => {
          const extended = places.map(p => {
            const placeDocRef = currentLevelPlacesRef.doc(`${p.placeId}`);
            const questionsColRef = placeDocRef.collection<IQuestion>(
              'questions',
              ref => ref.orderBy('question', 'asc')
            );

            p.questions = questionsColRef.valueChanges();

            return p;
          });

          return extended;
        })
        .switchMap(places => {
          // su `switchMap` papildome gautus duomenis - ar vartotojas yra jau
          // aplankęs užkrautą vietą.
          return placesUserVisited.map(pv => {
            const extended: IPlace[] = places.reduce((acc, place) => {
              place.visited = pv.indexOf(place.placeId) !== -1;
              acc.push(place);

              return acc;
            }, []);

            return extended;
          });
        });
    });

    // Get all UNDISCOWVERED places for current level
    this.undiscoveredPlaces = this.places.map(places =>
      places.filter(place => place.visited === false)
    );
    // Get all DISCOVERED place for current level
    this.discoveredPlaces = this.places.map(places =>
      places.filter(place => place.visited === true)
    );
  }

  updateAnsweredQuestions(count: number, placeId: string): Promise<void> {
    if (!count || !placeId) return;

    return new Promise(resolve => {
      this.user.subscribe(user => {
        const userDocRef = this.afs.collection('users').doc<IUser>(user.uid);

        userDocRef
          .set({
            ...user,
            placesVisited: Array.from(new Set(user.placesVisited).add(placeId)),
            questionsAnswered: user.questionsAnswered + count,
          })
          .then(() => {
            resolve();
          });
      });
    });
  }

  get users(): Observable<IUser[]> {
    return this.afs.collection<IUser>('users').valueChanges();
  }

  get leaderboard(): Observable<any> {
    return this.users.switchMap(users => {
      return this.user.map(user => {
        return users
          .filter(u => u.questionsAnswered > 0)
          .sort((a, b) => Number(a.questionsAnswered < b.questionsAnswered))
          .map(u => {
            const { level, questionsAnswered, displayName, photoURL } = u;

            return {
              displayName,
              level,
              me: u.uid === user.uid,
              photoURL,
              questionsAnswered,
            };
          });
      });
    });
  }

  get allLevels(): Observable<ILevelItem[]> {
    const levelsCol = this.afs.collection<ILevel>('levels');

    return (
      levelsCol
        .valueChanges()
        // Get all levels
        .switchMap(levels => {
          // Get places observables for every level
          const places$arr = levels.map((_, i) =>
            levelsCol
              .doc<ILevel>(`${i + 1}`)
              .collection<IPlace>('places', ref => ref.orderBy('name', 'asc'))
              .valueChanges()
              .map(places => {
                return {
                  level: i + 1,
                  places,
                };
              })
          );

          // Combine places observables to ILevelItem[] && filter empty
          return combineLatest<ILevelItem>(places$arr).map(li =>
            li.filter(p => p.places.length > 0)
          );
        })
    );
  }

  get allRemainingLevels(): Observable<ILevelItem[]> {
    return this.user.switchMap(user => {
      return this.allLevels.map(liArr =>
        liArr.filter(li => li.level > user.level)
      );
    });
  }

  authDialog(): void {
    this.dialog.open(AuthPageComponent);
  }
}
