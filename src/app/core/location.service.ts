import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/operator/bufferTime';
import 'rxjs/add/operator/throttle';
import { ILatLng } from '../../typings';

@Injectable()
export class LocationService {
  location: Observable<ILatLng>;
  lastLocation: ILatLng;
  watchId: number;
  watchOptions: PositionOptions = {
    enableHighAccuracy: true,
    maximumAge: 1000,
    timeout: 10000,
  };

  static toRadians(degrees: number): number {
    return degrees * Math.PI / 180;
  }

  /**
   * Paskaičiuoja atstumą metrais tarp dviejų koordinačių taškų.
   * @param a Koordintatė A
   * @param b Koordinatė B
   */
  static distance(a: ILatLng, b: ILatLng): number {
    const R = 6371e3; // metres
    const φ1 = LocationService.toRadians(a.lat);
    const φ2 = LocationService.toRadians(b.lat);
    const dφ = LocationService.toRadians(b.lat - a.lat);
    const dλ = LocationService.toRadians(b.lng - a.lng);

    const ang =
      Math.sin(dφ / 2) * Math.sin(dφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(dλ / 2) * Math.sin(dλ / 2);
    const c = Math.atan2(Math.sqrt(ang), Math.sqrt(1 - ang)) * 2;

    const d = R * c;

    return d;
  }

  static humanDistance(a: ILatLng, b: ILatLng): string {
    const distance = LocationService.distance(a, b);

    if (distance < 1000) {
      return `${Math.round(distance)} m`; // 153 m
    }

    return `${Math.round(distance * 100 / 1000) / 100} km`; // 1.53 km
  }

  constructor() {
    const location$: Observable<ILatLng> = Observable.create(observer => {
      if (navigator.geolocation) {
        if (this.lastLocation) observer.next(this.lastLocation);
        if (this.watchId) navigator.geolocation.clearWatch(this.watchId);

        this.watchId = navigator.geolocation.watchPosition(
          position => {
            const latLng = {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            };

            /**
             * Jeigu turime jau nustatytą vartotojo poziciją, patikrinkime ar
             * naujoji pozicija iš esmės skiriasi nuo anksčiau užregistruotos.
             */

            if (this.lastLocation) {
              // Jeigu naujoji pozicija viršija 10 metrų skirtumą...
              if (LocationService.distance(this.lastLocation, latLng) > 10) {
                // Atnaujiname pozicijos rodmenis
                this.lastLocation = latLng;
                observer.next(latLng);
              }
            } else {
              // Atnaujiname pozicijos rodmenis
              this.lastLocation = latLng;
              observer.next(latLng);
            }
          },
          err => {
            observer.error(err);
          },
          this.watchOptions
        );
      } else observer.error(`Browser can't use geolocation`);
    });

    this.location = location$;
  }
}
