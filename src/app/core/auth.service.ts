import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from 'angularfire2/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { IUser } from '../../typings';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class AuthService {
  user: Observable<IUser>;
  loading = true;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private dialog: MatDialog
  ) {
    // Get auth data, then get firestore user document || null
    this.user = this.afAuth.authState.switchMap(user => {
      if (user) return this.afs.doc<IUser>(`users/${user.uid}`).valueChanges();

      return Observable.of(null);
    });

    this.user.take(1).subscribe(() => {
      this.loading = false;
    });
  }

  logout(): Promise<void> {
    this.router.navigate(['']);

    return this.afAuth.auth.signOut().then(() => location.reload());
  }

  loginGoogle(): Promise<void> {
    const provider = new firebase.auth.GoogleAuthProvider();

    return this.oAuthLogin(provider);
  }

  loginFacebook(): Promise<void> {
    const provider = new firebase.auth.FacebookAuthProvider();

    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider): Promise<void> {
    return this.afAuth.auth
      .signInWithPopup(provider)
      .then(credential => {
        this.router.navigate(['']);
        this.dialog.closeAll();
        this.saveUserData(credential.user);
      })
      .catch(error => {
        console.error('oAuthLogin Error | ', error);
      });
  }

  // After successful login...
  private saveUserData(user): Subscription {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<IUser> = this.afs.doc(
      `users/${user.uid}`
    );

    return userRef.valueChanges().subscribe(dbUser => {
      let data: IUser = {
        displayName: user.displayName,
        email: user.email,
        level: user.level || 1,
        photoURL: user.photoURL,
        placesVisited: user.placesVisited || [],
        questionsAnswered: user.questionsAnswered || 0,
        uid: user.uid,
        ...dbUser,
      };

      if (user.providerData) {
        data.photoURL = user.providerData[0].photoURL;
        data.displayName = user.providerData[0].displayName;
      }

      return userRef.set(data);
    });
  }
}
