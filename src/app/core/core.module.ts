import { Location } from '@angular/common';
import { NgModule } from '@angular/core';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { CameraService } from './camera.service';
import { DataService } from './data.service';
import { LoadingService } from './loading.service';
import { LocationService } from './location.service';
import { NewLevelGuard } from './new-level.guard';
import { PlaceGuard } from './place.guard';

@NgModule({
  declarations: [],
  imports: [AngularFireAuthModule, AngularFirestoreModule],
  providers: [
    AuthService,
    AuthGuard,
    PlaceGuard,
    DataService,
    LocationService,
    CameraService,
    LoadingService,
    Location,
    NewLevelGuard,
  ],
})
export class CoreModule {}
