import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {
  loading: boolean;
  constructor() {
    this.loading = true;
  }
  start(): void {
    this.loading = true;
  }
  stop(): void {
    this.loading = false;
  }
  toggle(): void {
    this.loading = !this.loading;
  }
}
