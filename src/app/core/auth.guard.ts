import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { AuthPageComponent } from '../pages/auth-page/auth-page.component';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AuthService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.auth.user
      .take(1)
      .map(user => !!user)
      .do(loggedIn => {
        if (!loggedIn) {
          console.info('Auth Guard | Access denied');
          this.router.navigate(['zemelapis']);
          this.dialog.open(AuthPageComponent);
        }
      });
  }
}
