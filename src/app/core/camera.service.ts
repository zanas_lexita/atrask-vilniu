import { Injectable, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment.prod';

// Global variables type fix
declare var ARController: any;
declare var artoolkit: any;
declare var THREE: any;

@Injectable()
export class CameraService implements OnDestroy {
  active: boolean;

  constructor() {
    this.active = true;
  }

  destroy(): void {
    this.active = false;
    console.info('AR destroyed');
  }

  activate(): void {
    this.active = true;
    console.info('AR activated');
  }

  ngOnDestroy(): void {
    this.destroy();
  }

  detection(element: HTMLElement): Observable<any> {
    const longSide = () => {
      const w = width();
      const h = height();
      if (w > h) return w;

      return h;
    };

    const width = () => element.offsetWidth;
    const height = () => element.offsetHeight;

    const { marker, camera } = environment.ar;

    return Observable.create(observer => {
      ARController.getUserMediaThreeScene({
        cameraParam: camera,
        facing: 'environment',
        maxARVideoSize: longSide(),
        onError: () => {
          observer.error('Nepavyko sukurti AR scenos');
        },
        onSuccess: (arScene, arController) => {
          element.className = arController.orientation;
          1;

          const isMobile = /Android|mobile|iPad|iPhone/i.test(
            navigator.userAgent
          );

          // 1. Canvas setup
          // Initialize WebGL Canvas
          const renderer = new THREE.WebGLRenderer({ antialias: true });

          if (arController.orientation === 'portrait') {
            const w =
              width() / arController.videoHeight * arController.videoWidth;
            const h = width();
            renderer.setSize(w, h);
            // renderer.domElement.style.paddingBottom = `${w - h}px`;
          } else {
            // Landscape
            if (isMobile) {
              // Landscape mobile
              renderer.setSize(
                width(),
                width() / arController.videoWidth * arController.videoHeight
              );
            } else {
              // Landscape desktop
              // renderer.setSize(arController.videoWidth, arController.videoHeight);
              renderer.setSize(
                width(),
                width() / arController.videoWidth * arController.videoHeight
              );
              element.className += ' desktop';
            }
          }

          // Append canvas
          element.appendChild(renderer.domElement);

          let mesh = null;

          const loader = new THREE.JSONLoader();
          loader.load('/assets/3d_model.json', (geometry, materials) => {
            mesh = new THREE.Mesh(
              geometry,
              new THREE.MeshFaceMaterial(materials)
            );
            if (!isMobile) {
              mesh.rotation.y = Math.PI * 180 / 180;
            }
            mesh.scale.x = mesh.scale.y = mesh.scale.z = 0.8;
            mesh.position.z = 0.25;

            const ambLight = new THREE.AmbientLight(0xffffff, 0.8);
            const light = new THREE.DirectionalLight(0xffffff, 0.5);
            light.position.x = 0;
            light.position.y = 0;
            light.position.z = 0.2;
            // mesh.material.shading = THREE.FlatShading;

            arController.loadMarker(marker, markerId => {
              const markerRoot = arController.createThreeMarker(markerId);

              markerRoot.add(mesh);
              arScene.scene.add(markerRoot);
              arScene.scene.add(ambLight);
              arScene.scene.add(light);
            });
          });
          arController.addEventListener('getMarker', ev => {
            let obj;

            if (ev.data.type === artoolkit.PATTERN_MARKER) {
              obj = arController.threePatternMarkers[ev.data.marker.idPatt];
            } else if (ev.data.type === artoolkit.BARCODE_MARKER) {
              obj = arController.threeBarcodeMarkers[ev.data.marker.idMatrix];
            }

            if (obj) {
              // console.log('found marker?', ev.data.marker);
              observer.next();
            }
          });

          // 3. Canvas animation loop

          const tick = () => {
            arScene.process();

            arScene.renderOn(renderer);
            setTimeout(() => {
              if (this.active === false) return;
              requestAnimationFrame(tick);
            }, 50);
          };

          this.activate();

          tick();
        },
      });
    })
      .bufferTime(200)
      .filter(detects => detects.length > 0);
  }
}
