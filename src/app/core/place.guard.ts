import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/combineLatest';
import { DataService } from './data.service';

@Injectable()
export class PlaceGuard implements CanActivate {
  constructor(private router: Router, private data: DataService) {}

  canActivate(
    next: ActivatedRouteSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const openMap = () => this.router.navigate(['zemelapis']);

    const placeId = next.params.placeId;

    // Jeigu neturime placeId...
    if (!placeId) {
      openMap();

      return false;
    }

    return Observable.combineLatest(this.data.places, this.data.user)
      .take(1)
      .map(combined => {
        const places = combined[0];
        const user = combined[1];

        // Jeigu bando atidaryti ne dabartinio lygio vietą arba jau aplankė šią vietą...
        if (
          places.map(p => p.placeId).indexOf(placeId) === -1 ||
          user.placesVisited.indexOf(placeId) !== -1
        ) {
          // Nukreipiame į žemėlapį
          openMap();

          return false;
        }

        return true;
      });
  }
}
