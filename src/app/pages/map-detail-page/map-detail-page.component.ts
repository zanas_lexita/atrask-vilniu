import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { IPlace, IUser } from '../../../typings';
import { DataService } from '../../core/data.service';
import { LocationService } from '../../core/location.service';

@Component({
  selector: 'app-map-detail-page',
  styleUrls: ['./map-detail-page.component.scss'],
  templateUrl: './map-detail-page.component.html',
})
export class MapDetailPageComponent implements OnInit {
  place$: Observable<IPlace>;
  user$: Observable<IUser>;
  distance?: string;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public data: DataService,
    public dialog: MatDialog,
    public ls: LocationService
  ) {}

  ngOnInit(): void {
    this.user$ = this.data.user;
    this.place$ = this.route.paramMap
      .map(paramMap => {
        return paramMap.get('placeId');
      })
      .switchMap(placeId => {
        return this.data.places
          .map(places => {
            return places.filter(p => p.placeId === placeId)[0];
          })
          .do(place => {
            if (this.ls.lastLocation) {
              this.distance = LocationService.humanDistance(
                this.ls.lastLocation,
                {
                  lat: place.coordinates.latitude,
                  lng: place.coordinates.longitude,
                }
              );
            }
          });
      });
  }

  openRoute(placeId: string): void {
    this.router
      .navigate([
        {
          outlets: {
            overlay: null,
          },
        },
      ])
      .then(() => {
        this.router.navigate(['/zemelapis/', placeId]);
      });
  }
}
