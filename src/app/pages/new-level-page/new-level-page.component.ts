import {
  animate,
  query,
  stagger,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { ILevelData, IPlace } from '../../../typings';
import { DataService } from '../../core/data.service';
import { LoadingService } from '../../core/loading.service';

@Component({
  animations: [
    trigger('sectionsIn', [
      transition('* => *', [
        query(
          '.section',
          style({ opacity: 0, transform: 'transformY(-12px)' })
        ),
        query(
          '.section',
          stagger('300ms', [
            animate(
              '800ms 50ms ease-in',
              style({ opacity: 1, transform: 'transformY(0)' })
            ),
          ])
        ),
      ]),
    ]),
  ],
  selector: 'app-new-level-page',
  styleUrls: ['./new-level-page.component.scss'],
  templateUrl: './new-level-page.component.html',
})
export class NewLevelPageComponent implements OnInit {
  newPlaces: IPlace[];
  levelData: ILevelData;
  constructor(public data: DataService, private lds: LoadingService) {}

  ngOnInit(): void {
    this.lds.start();
    combineLatest(this.data.places, this.data.levelData)
      .take(1)
      .subscribe(combined => {
        this.newPlaces = combined[0];
        this.levelData = combined[1];
        this.lds.stop();
      });
  }
}
