import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import { IQuestion, IUser } from '../../../typings';
import { DataService } from '../../core/data.service';
import { LoadingService } from '../../core/loading.service';

@Component({
  selector: 'app-quiz-page',
  styleUrls: ['./quiz-page.component.scss'],
  templateUrl: './quiz-page.component.html',
})
export class QuizPageComponent implements OnInit {
  placeId$: Observable<string>;
  placeId: string;
  questions$: Observable<IQuestion[]>;
  questions: IQuestion[];
  selected = [];
  valid = false;
  showResults = false;

  constructor(
    public data: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private lds: LoadingService,
    private afs: AngularFirestore
  ) {
    // Get placeID from URL
    this.questions$ = this.route.paramMap
      .map(paramMap => {
        return paramMap.get('placeId');
      })
      .switchMap(placeId => {
        this.placeId = placeId;

        return data.places
          .map(places => {
            return places.filter(p => p.placeId === placeId)[0];
          })
          .switchMap(place => place.questions);
      });
  }

  ngOnInit(): void {
    this.lds.start();
    this.questions$.take(1).subscribe(questions => {
      this.questions = questions;
      this.selected = Array(questions.length).fill(undefined);
      this.lds.stop();
    });
  }

  onChange(): void {
    this.valid =
      this.selected.filter(v => v !== undefined).length ===
      this.questions.length;
  }

  finishQuiz(): void {
    const count = this.questions.reduce(
      (acc, q, j) => (q.correctAnswer === this.selected[j] ? ++acc : acc),
      0
    );

    this.lds.start();
    let userDoc: AngularFirestoreDocument<IUser>;

    // setTimeout ffor loader to update
    setTimeout(() => {
      this.data.user
        .take(1)
        .switchMap(user => {
          // Update places visited
          userDoc = this.afs.collection('users').doc<IUser>(user.uid);

          return Observable.fromPromise(
            userDoc.update({
              placesVisited: Array.from(
                new Set(user.placesVisited).add(this.placeId)
              ),
              questionsAnswered: count + user.questionsAnswered,
            })
          );
        })
        .take(1)
        .switchMap(() => {
          // Update user level if eligible
          // 1. Get level data
          return this.data.levelData.switchMap(levelData => {
            // If remaining to next level = 0...
            // Update user level
            if (levelData.nextRemaining === 0) return Observable.of(true);

            return Observable.of(false);
          });
        })
        .take(1)
        .subscribe(newLevel => {
          if (newLevel === true) {
            this.router.navigate(['/naujas-lygis']).then(() => {
              this.lds.stop();
            });
          } else {
            this.router.navigate(['/zemelapis']).then(() => {
              this.lds.stop();
            });
          }
        });
    }, 0);
  }
}
