import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { LoadingService } from '../../core/loading.service';
import { DataService } from './../../core/data.service';
import { IUserData, ILevelItem } from '../../../typings';

@Component({
  selector: 'app-user-page',
  styleUrls: ['./user-page.component.scss'],
  templateUrl: './user-page.component.html',
})
export class UserPageComponent implements OnInit {
  user: IUserData;
  progress: number;
  levels: ILevelItem[];

  constructor(public data: DataService, private lds: LoadingService) {}

  ngOnInit(): void {
    this.lds.start();
    combineLatest(
      this.data.userData,
      this.data.progress,
      this.data.allRemainingLevels
    )
      .take(1)
      .subscribe(combined => {
        this.user = combined[0];
        this.progress = combined[1];
        this.levels = combined[2];
        this.lds.stop();
      });
  }
}
