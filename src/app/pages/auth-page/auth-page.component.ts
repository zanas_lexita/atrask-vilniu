import { Component } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-auth-page',
  styleUrls: ['./auth-page.component.scss'],
  templateUrl: './auth-page.component.html',
})
export class AuthPageComponent {
  constructor(public auth: AuthService) {}
}
