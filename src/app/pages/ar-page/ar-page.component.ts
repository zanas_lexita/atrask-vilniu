import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/takeWhile';

@Component({
  selector: 'app-ar-page',
  styleUrls: ['./ar-page.component.scss'],
  templateUrl: './ar-page.component.html',
})
export class ArPageComponent {
  placeId$: Observable<string>;
  timer = null;

  constructor(private route: ActivatedRoute, private router: Router) {
    this.placeId$ = this.route.paramMap.map(paramMap => {
      return paramMap.get('placeId');
    });
  }

  onDetected(): void {
    this.placeId$.subscribe(id => {
      this.timer = 3;

      // return; // DEBUG

      Observable.interval(1000)
        .takeWhile(() => this.timer > 1)
        .subscribe(() => (this.timer -= 1), null, () => {
          this.router.navigate([
            '/vieta',
            {
              placeId: id,
            },
          ]);
        });
    });
  }
}
