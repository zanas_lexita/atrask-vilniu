import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { ActivatedRoute, ActivationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/combineLatest';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/last';
import { IPlace } from '../../../typings';
import { DataService } from '../../core/data.service';
import { MapComponent } from '../../ui/map/map.component';

@Component({
  selector: 'app-map-page',
  styleUrls: ['./map-page.component.scss'],
  templateUrl: './map-page.component.html',
})
export class MapPageComponent implements OnInit, OnDestroy {
  @ViewChild('map') map: MapComponent;
  @ViewChild('snav') snav: MatSidenav;

  routedPlaceId: string;
  changedRoute$: Observable<string>;
  changedRouteSub: Subscription;

  constructor(
    public data: DataService,
    public router: Router,
    private route: ActivatedRoute
  ) {}

  routeCancel(): void {
    this.routedPlaceId = '';
    this.router.navigate(['/zemelapis']);
  }

  listenToRoutes(): void {
    this.changedRouteSub = this.changedRoute$.subscribe(placeId => {
      this.map.showRoute(placeId);
    });
  }

  ngOnInit(): void {
    // On map route change... (based on URL)
    this.changedRoute$ = Observable.merge(
      this.router.events,
      Observable.of(this.route)
    )
      // Current URL (ActivatedRoute) and changed URL (ActivationEnd) are needed
      .switchMap(e => {
        if (e instanceof ActivatedRoute) {
          return e.params;
        } else if (e instanceof ActivationEnd) {
          return Observable.of(e.snapshot.params);
        }

        return Observable.of(null);
      })
      // Filter irrelevant route changes
      .filter(e => e !== null && e.id !== undefined)
      // Get the ID from URL
      .map(e => e.id)
      // Filter only DIFFERENT ID
      .filter(placeId => placeId !== this.routedPlaceId)
      // Save NEW & DIFERRENT ID
      .do(placeId => {
        // Definitely new route
        this.routedPlaceId = placeId;
      });
  }

  ngOnDestroy(): void {
    if (this.changedRouteSub) {
      this.changedRouteSub.unsubscribe();
    }
  }

  openARPage(place: IPlace): void {
    this.router.navigate([
      '/kamera',
      {
        placeId: place.placeId,
      },
    ]);
  }

  openPlace(place: IPlace): void {
    if (this.routedPlaceId) {
      return;
    }
    const url = this.router.createUrlTree([
      {
        outlets: {
          overlay: ['place', { placeId: place.placeId }],
        },
      },
    ]);
    this.router.navigateByUrl(url);
  }
}
