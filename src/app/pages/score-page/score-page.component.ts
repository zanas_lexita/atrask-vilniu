import { Component, OnInit } from '@angular/core';
import { DataService } from '../../core/data.service';

@Component({
  selector: 'app-score-page',
  styleUrls: ['./score-page.component.scss'],
  templateUrl: './score-page.component.html',
})
export class ScorePageComponent implements OnInit {
  leaderboard$: any;
  constructor(private data: DataService) {}

  ngOnInit(): void {
    this.leaderboard$ = this.data.leaderboard;
  }
}
