/**
 * Čia yra nurodyti visi komponentai, kurie naudojami tiek `app.module.ts`, tiek `app-routing.module.ts`.
 */
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AboutPageComponent } from '../pages/about-page/about-page.component';
import { ArPageComponent } from '../pages/ar-page/ar-page.component';
import { AuthPageComponent } from '../pages/auth-page/auth-page.component';
import { MapDetailPageComponent } from '../pages/map-detail-page/map-detail-page.component';
import { MapPageComponent } from '../pages/map-page/map-page.component';
import { NewLevelPageComponent } from '../pages/new-level-page/new-level-page.component';
import { QuizPageComponent } from '../pages/quiz-page/quiz-page.component';
import { ScorePageComponent } from '../pages/score-page/score-page.component';
import { UserPageComponent } from '../pages/user-page/user-page.component';
import { ArComponent } from '../ui/ar/ar.component';
import { LoaderComponent } from '../ui/loader/loader.component';
import { MapComponent } from '../ui/map/map.component';
import { NavLinkComponent } from '../ui/nav-link/nav-link.component';
import { PlaceCardComponent } from '../ui/place-card/place-card.component';
import { PlaceListComponent } from '../ui/place-list/place-list.component';
import { MaterialComponentsModule } from './material.components.module';

@NgModule({
  declarations: [
    // Pages
    AboutPageComponent,
    ArPageComponent,
    AuthPageComponent,
    MapDetailPageComponent,
    MapPageComponent,
    NewLevelPageComponent,
    QuizPageComponent,
    ScorePageComponent,
    UserPageComponent,
    // Components
    ArComponent,
    LoaderComponent,
    MapComponent,
    NavLinkComponent,
    PlaceCardComponent,
    PlaceListComponent,
  ],
  // Dialogs
  entryComponents: [AuthPageComponent],
  exports: [
    MaterialComponentsModule,
    ReactiveFormsModule,
    NavLinkComponent,
    LoaderComponent,
  ],
  imports: [
    MaterialComponentsModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
  ],
})
export class SharedModule {}
