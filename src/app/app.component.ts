import { MediaMatcher } from '@angular/cdk/layout';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { IUser } from '../typings';
import { guestRoutes, registeredRoutes } from './app-routing.module';
import { AuthService } from './core/auth.service';
import { DataService } from './core/data.service';
import { LoadingService } from './core/loading.service';
import { AuthPageComponent } from './pages/auth-page/auth-page.component';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
})
export class AppComponent implements OnDestroy {
  @ViewChild('snav') snav: any;
  user: IUser;

  registeredMainRoutes = registeredRoutes.filter(r => r.data && r.data.main);
  registeredOtherRoutes = registeredRoutes.filter(r => r.data && !r.data.main);

  unregisteredMainRoutes = guestRoutes.filter(r => r.data && r.data.main);
  unregisteredOtherRoutes = guestRoutes.filter(r => r.data && !r.data.main);

  mobileQuery: MediaQueryList;
  title = 'Atrask Vilnių!';
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public router: Router,
    public auth: AuthService,
    public data: DataService,
    public dialog: MatDialog,
    public lds: LoadingService
  ) {
    lds.start();

    data.user.subscribe(user => {
      this.user = user;
      lds.stop();
    });

    router.events.subscribe((r: any) => {
      if (!r.snapshot || (r.snapshot && !r.snapshot.data.title)) return;
      this.title = r.snapshot.data.title;
    });

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    // console.log(this.mobileQuery);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  closeSideNav(): void {
    // If mobile... Close sidenav
    if (this.mobileQuery.matches) this.snav.close();
  }

  openAuthDialog(): void {
    this.dialog.open(AuthPageComponent);
  }
}
