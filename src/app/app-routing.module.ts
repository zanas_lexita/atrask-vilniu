import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { PlaceGuard } from './core/place.guard';
// Pages
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { ArPageComponent } from './pages/ar-page/ar-page.component';
import { MapDetailPageComponent } from './pages/map-detail-page/map-detail-page.component';
import { MapPageComponent } from './pages/map-page/map-page.component';
import { QuizPageComponent } from './pages/quiz-page/quiz-page.component';
import { ScorePageComponent } from './pages/score-page/score-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { NewLevelGuard } from './core/new-level.guard';
import { NewLevelPageComponent } from './pages/new-level-page/new-level-page.component';

export const guestRoutes: Routes = [
  {
    component: MapPageComponent,
    data: {
      icon: 'map',
      title: 'Žemėlapis',
    },
    path: 'zemelapis',
  },
  {
    component: AboutPageComponent,
    data: {
      icon: 'help_outline',
      main: false,
      title: 'Apie programėlę',
    },
    path: 'apie',
  },
  {
    component: MapDetailPageComponent,
    data: {
      hidden: true,
      icon: 'help_outline',
      main: false,
      title: 'Vieta',
    },
    outlet: 'overlay',
    path: 'place',
  },
];

export const registeredRoutes: Routes = [
  {
    component: MapPageComponent,
    data: {
      icon: 'map',
      main: true,
      title: 'Žemėlapis',
    },
    path: 'zemelapis/:id',
  },
  {
    canActivate: [AuthGuard],
    component: UserPageComponent,
    data: {
      icon: 'stars',
      main: true,
      title: 'Mano progresas',
    },
    path: 'progresas',
  },
  {
    canActivate: [AuthGuard],
    component: ScorePageComponent,
    data: {
      icon: 'supervisor_account',
      main: true,
      title: 'Lyderių lentelė',
    },
    path: 'lyderiai',
  },
  {
    canActivate: [AuthGuard, PlaceGuard],
    component: ArPageComponent,
    data: {
      hidden: true,
      title: 'Kamera',
    },
    path: 'kamera',
  },
  {
    canActivate: [AuthGuard, PlaceGuard],
    component: QuizPageComponent,
    data: {
      hidden: true,
      title: 'Vieta',
    },
    path: 'vieta',
  },
  {
    canActivate: [AuthGuard, NewLevelGuard],
    component: NewLevelPageComponent,
    data: {
      hidden: true,
      title: 'Naujas lygis',
    },
    path: 'naujas-lygis',
  },
  {
    component: AboutPageComponent,
    data: {
      icon: 'help_outline',
      title: 'Apie programėlę',
    },
    path: 'apie',
  },
];

export const routes: Routes = [
  ...guestRoutes,
  ...registeredRoutes,
  {
    path: '**',
    pathMatch: 'prefix',
    redirectTo: '/zemelapis',
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/zemelapis',
  },
];

@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule {}
