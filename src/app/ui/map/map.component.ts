import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import { ILatLng, IPlace, IPlaceMarker } from '../../../typings';
import { LocationService } from './../../core/location.service';
import { styles } from './map.style';

@Component({
  selector: 'app-map',
  styleUrls: ['./map.component.scss'],
  templateUrl: './map.component.html',
})
export class MapComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('map') element: any;
  @Input() places: IPlace[];
  @Output() markerClick = new EventEmitter<IPlace>();
  @Output() routeCancel = new EventEmitter<string>();
  @Output() markersPlaced = new EventEmitter<string>();
  loading = true;
  // Map config
  map: google.maps.Map;
  mapConfig: google.maps.MapOptions;
  // Map objects
  markers: IPlaceMarker[] = [];
  selected: IPlace;
  // Geolocation data
  location: Observable<ILatLng>;
  locationSub: Subscription;
  userMarker: google.maps.Marker;
  watchId: number;
  // Routing data
  routeInfo: any;
  routeMarkers: google.maps.Marker[];
  dService: google.maps.DirectionsService;
  dRenderer: google.maps.DirectionsRenderer;
  dResult: google.maps.DirectionsResult;
  dWindow: google.maps.InfoWindow;
  // GeoFence
  inRange = false;

  constructor(private snackBar: MatSnackBar, private ls: LocationService) {
    this.mapConfig = {
      center: new google.maps.LatLng(54.687157, 25.279652),
      disableDefaultUI: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      maxZoom: 19,
      minZoom: 12,
      styles,
      zoom: 13,
    };
    this.location = this.ls.location
      .do(location => {
        this.loading = false;
        console.info('location update', location);
        this.setUserMarker(location);
      })
      .pipe(
        catchError(err => {
          this.onLocationError(err);

          return Observable.of({
            lat: 54.69582450258429,
            lng: 25.26356265397294,
          });
        })
      );
  }

  /**
   * Implements
   */

  ngOnInit(): void {
    // Instantiate a map
    this.map = new google.maps.Map(this.element.nativeElement, this.mapConfig);
    // Instantiate a directions service.
    this.dService = new google.maps.DirectionsService();
    this.setEvents();
  }

  ngOnChanges(changes): void {
    this.clearMarkers();
    if (changes.places && changes.places.currentValue) {
      this.setMarkers(this.places);
      this.fitMarkers();
      this.loading = false;
    }
  }

  ngOnDestroy(): void {
    if (this.watchId) navigator.geolocation.clearWatch(this.watchId);
    if (this.locationSub) this.locationSub.unsubscribe();
  }

  /**
   * Parodo vartotojo poziciją žemėlapyje
   */
  locate(): Subscription {
    this.info('Bandome nustatyti jūsų buvimo vietą...', 1500);

    return this.location.take(1).subscribe(location => {
      this.map.setCenter(location);
    });
  }

  /**
   * Sutalpina žemėlapyje esančius žymeklius ekrane
   */
  fitMarkers(): void {
    const bounds = new google.maps.LatLngBounds();
    this.markers.forEach(m => bounds.extend(m.getPosition()));
    this.map.fitBounds(bounds);
  }

  /**
   * Panaikina rodomą žemėlapio maršrutą ir grąžina pasleptus žymeklius.
   */
  cancelRoute(): void {
    this.hideAllMarkers(this.routeMarkers);
    this.showAllMarkers();
    this.dRenderer.setMap(null);
    this.routeInfo = null;
    this.fitMarkers();

    this.routeCancel.emit();
  }

  /**
   * Atvaizduoja maršrutą iki nurodytos vietos
   * @param placeId Vietos ID
   */
  showRoute(placeId: string): void {
    if (!placeId) return;

    this.locationSub = this.location.subscribe(startUser => {
      const endTarget = this.soloMarker(placeId);
      const endTargetPos = endTarget.getPosition();
      const place = endTarget.place;
      const map = this.map;

      // If we already have a route showing...
      // Remove existing route (when user position updates)
      if (this.dRenderer) this.dRenderer.setMap(null);

      // Create a renderer for directions and bind it to the map.
      this.dRenderer = new google.maps.DirectionsRenderer({
        map,
        polylineOptions: {
          strokeColor: '#009688',
        },
        preserveViewport: true,
        suppressMarkers: true,
      });
      // Instantiate an info window to hold step text.
      this.dWindow = new google.maps.InfoWindow();

      // Display the route between the initial start and end selections.
      // Retrieve the start and end locations and create a DirectionsRequest using
      // WALKING directions.
      this.dService.route(
        {
          destination: endTargetPos,
          origin: startUser,
          region: 'lt',
          travelMode: google.maps.TravelMode.WALKING,
          unitSystem: google.maps.UnitSystem.METRIC,
        },
        (response, status) => {
          // Route the directions and pass the response to a function to create
          // markers for each step.
          if (status.toString() === 'OK') {
            this.dResult = response;
            this.dRenderer.setDirections(response);
            // Update route information
            this.inRange =
              response.routes[0].legs[0].distance.value < (place.radius || 25);

            // Priartintiname žemėlapio vaizdą prie maršruto
            if (!this.routeInfo) this.map.fitBounds(response.routes[0].bounds);

            this.routeInfo = {
              distance: response.routes[0].legs[0].distance,
              duration: response.routes[0].legs[0].duration,
              endTarget,
              place,
              response,
            };

            // this.showRouteSteps();
          } else window.alert(`Directions request failed due to ${status}`);
        }
      );
    });

    // function attachInstructionText(stepDisplay, marker, text, map) {
    //   google.maps.event.addListener(marker, 'click', () => {
    //     // Open an info window when the marker is clicked on, containing the text
    //     // of the step.
    //     stepDisplay.setContent(text);
    //     stepDisplay.open(map, marker);
    //   });
    // }
  }

  private setEvents(): void {
    google.maps.event.addListener(this.map, 'click', event => {
      alert(
        `Latitude: ${event.latLng.lat()}, Longitude: ${event.latLng.lng()}`
      );
    });
  }

  private setMarkers(places: IPlace[]): void {
    const map = this.map;
    places.map((place, i) => {
      const imagePath =
        place.visited === true
          ? '/assets/images/markers/SVG/marker_done.svg'
          : '/assets/images/markers/SVG/marker.svg';
      const image: google.maps.Icon = {
        anchor: new google.maps.Point(95 / 4 / 2, 128 / 4),
        origin: new google.maps.Point(0, 0),
        scaledSize: new google.maps.Size(95 / 4, 128 / 4),
        size: new google.maps.Size(95 / 4, 128 / 4),
        url: imagePath,
      };

      const position = new google.maps.LatLng(
        place.coordinates.latitude,
        place.coordinates.longitude
      );
      const marker: any = new google.maps.Marker({
        // animation: google.maps.Animation.DROP,
        icon: image,
        map,
        position,
        title: place.name,
        zIndex: 100,
      });
      marker.place = place;

      marker.addListener('click', () => {
        // this.map.setCenter(position);
        this.markerClick.emit(place);
      });
      this.markers.push(marker);
      if (i === places.length - 1) this.markersPlaced.emit();
    });
  }

  /**
   * Panaikina žemėlapio žymeklius
   */
  private clearMarkers(): void {
    // for example after login, after logout
    this.markers.forEach(m => m.setMap(null));
    this.markers = [];
  }

  /**
   * Prideda vartotojo pozicijos žymeklį žemėlapyje
   * @param position vartotojo koordinatės
   */
  private setUserMarker(position: ILatLng): void {
    if (this.userMarker) this.userMarker.setMap(null);
    const icon: google.maps.Icon = {
      anchor: new google.maps.Point(12, 12),
      origin: new google.maps.Point(0, 0),
      scaledSize: new google.maps.Size(24, 24),
      size: new google.maps.Size(24, 24),
      url: '/assets/images/markers/SVG/animated.png',
    };
    this.userMarker = new google.maps.Marker({
      icon,
      map: this.map,
      position,
      zIndex: 1000,
    });
  }

  private onLocationError(error?: PositionError): void {
    const { code } = error;
    switch (code) {
      case 1: // Leidimo nėra
        this.info('Reikia jūsų leidimo nustatyti jūsų poziciją');
        break;
      case 2: // Position unavailable. Location provider error
      case 3: // Timed out
        this.info('Nepavyko nustatyti jūsų vietos. Bandykite dar kartą.');
        break;
      default:
        this.info('Nepavyko nustatyti jūsų vietos');
    }

    console.error('Location error: ', error);
  }

  /**
   * Parodo informacinį pranešimą ekrane
   * @param message Informacinis pranešimas
   * @param duration Trukmė
   */
  private info(message: string, duration = 3000): void {
    this.snackBar.open(message, null, {
      duration,
      horizontalPosition: 'start',
    });
  }

  /*
  private showRouteSteps(): void {
    if (!this.dResult || !this.routeMarkers.length) return;
    // For each step, place a marker, and add the text to the marker's infowindow.
    // Also attach the marker to an array so we can keep track of it and remove it
    // when calculating new routes.
    const myRoute = this.dResult.routes[0].legs[0];
    for (let i = 0; i < myRoute.steps.length; i++) {
      const marker = (this.routeMarkers[i] =
        this.routeMarkers[i] || new google.maps.Marker());
      marker.setMap(this.map);
      marker.setPosition(myRoute.steps[i].start_location);

      attachInstructionText(
        stepDisplay,
        marker,
        myRoute.steps[i].instructions,
        map
      );

    }
  }
  */

  /**
   * Izoliuoja tam tikrą žymeklį pagal vietos ID (paslepia kitus)
   * @param placeId - vietos ID
   */
  private soloMarker(placeId: string): IPlaceMarker {
    this.hideAllMarkers();
    const soloMarker = this.markers.filter(m => m.place.placeId === placeId)[0];
    soloMarker.setMap(this.map);

    return soloMarker;
  }

  /**
   * Sudeda visus paduotus žymelius ant žemėlapio
   * @param markerArray Žymeklių masyvas
   */
  private showAllMarkers(
    markerArray: google.maps.Marker[] = this.markers
  ): void {
    markerArray.forEach(m => m.setMap(this.map));
  }

  /**
   * Paslepia visus paduotus žymelius ant žemėlapio
   * @param markerArray Žymeklių masyvas
   */
  private hideAllMarkers(
    markerArray: Array<google.maps.Marker> = this.markers
  ): void {
    markerArray.forEach(m => m.setMap(null));
  }
}
