import {
  Component,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/bufferTime';
import { CameraService } from './../../core/camera.service';

@Component({
  selector: 'app-ar',
  styleUrls: ['./ar.component.scss'],
  templateUrl: './ar.component.html',
})
export class ArComponent implements OnInit, OnDestroy {
  @ViewChild('ar') element: ElementRef;
  @Output() detect = new EventEmitter();
  cs: CameraService;
  detect$: Observable<any>;
  renderer: any;

  constructor(cs: CameraService) {
    this.cs = cs;
  }

  // Inicijavus papildytos realybės kameros komponentą...
  ngOnInit(): void {
    // CameraService detection
    this.cs
      .detection(this.element.nativeElement) // Grąžina atpažinto žymeklio srautą
      .take(1) // Reikia tik vieno atpažinimo kadro
      .subscribe(() => {
        // Šioje eilutėje žymeklis yra atpažintas
        // Skleidžiame įvykį, kad pavyko atpažinti elementą
        this.detect.emit();
      });
  }

  ngOnDestroy(): void {
    this.cs.destroy();
  }
}
