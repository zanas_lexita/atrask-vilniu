import { Component, Input } from '@angular/core';
import { IPlace } from '../../../typings';
import { Router } from '@angular/router';

@Component({
  selector: 'app-place-card',
  styleUrls: ['./place-card.component.scss'],
  templateUrl: './place-card.component.html',
})
export class PlaceCardComponent {
  @Input() place: IPlace;

  constructor(private router: Router) {}

  openPlace(place: IPlace): void {
    const url = this.router.createUrlTree([
      {
        outlets: {
          overlay: ['place', { placeId: place.placeId }],
        },
      },
    ]);
    this.router.navigateByUrl(url);
  }
}
