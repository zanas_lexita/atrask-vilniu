import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-nav-link',
  styleUrls: ['./nav-link.component.scss'],
  templateUrl: './nav-link.component.html',
})
export class NavLinkComponent {
  @Input() routes;
  @Input() title;
  @Input() icon;
  @Output() click = new EventEmitter();
}
