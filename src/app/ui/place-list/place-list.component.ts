import {
  animate,
  query,
  stagger,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input } from '@angular/core';
import { IPlace } from '../../../typings';

@Component({
  animations: [
    trigger('itemsAnimation', [
      transition('* => *', [
        query(
          '.image-container',
          style({ opacity: 0, transform: 'translateY(-32px)' })
        ),
        query(
          '.image-container',
          stagger('100ms', [
            animate(
              '600ms 300ms ease-in-out',
              style({ opacity: 1, transform: 'translateX(0)' })
            ),
          ])
        ),
      ]),
    ]),
  ],
  selector: 'app-place-list',
  styleUrls: ['./place-list.component.scss'],
  templateUrl: './place-list.component.html',
})
export class PlaceListComponent {
  @Input() places: IPlace[];
}
