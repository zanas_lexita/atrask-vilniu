import { Component } from '@angular/core';
import { LoadingService } from '../../core/loading.service';

@Component({
  selector: 'app-loader',
  styleUrls: ['./loader.component.scss'],
  templateUrl: './loader.component.html',
})
export class LoaderComponent {
  constructor(private lds: LoadingService) {}

  get stateName(): string {
    return this.lds.loading ? 'show' : 'hide';
  }
}
