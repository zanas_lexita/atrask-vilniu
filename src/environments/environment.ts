// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  ar: {
    camera: '/assets/ar/camera_para.dat',
    marker: '/assets/ar/marker.patt',
  },
  firebase: {
    apiKey: 'AIzaSyCFryUrTlamLgNns9yCd45Y8vgB4_gG698',
    authDomain: 'atrask-vilniu.firebaseapp.com',
    databaseURL: 'https://atrask-vilniu.firebaseio.com',
    messagingSenderId: '381125276535',
    projectId: 'atrask-vilniu',
    storageBucket: 'atrask-vilniu.appspot.com',
  },
  googleMapsKey: 'AIzaSyDoPcJcgyb_lxWJg9eVXt4Rsy9FoIp00LA',
  production: false,
};
