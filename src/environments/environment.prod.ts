export const environment = {
  ar: {
    camera: '/assets/ar/camera_para.dat',
    marker: '/assets/ar/marker.patt',
  },
  firebase: {
    apiKey: 'AIzaSyCFryUrTlamLgNns9yCd45Y8vgB4_gG698',
    authDomain: 'atrask-vilniu.firebaseapp.com',
    databaseURL: 'https://atrask-vilniu.firebaseio.com',
    messagingSenderId: '381125276535',
    projectId: 'atrask-vilniu',
    storageBucket: 'atrask-vilniu.appspot.com',
  },
  googleMapsKey: 'AIzaSyDoPcJcgyb_lxWJg9eVXt4Rsy9FoIp00LA',
  production: true,
};
