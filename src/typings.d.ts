import { GeoPoint } from '@firebase/firestore-types';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

interface IPlace {
  coordinates: GeoPoint;
  name: string;
  photoURL: string;
  description: string;
  placeId: string;
  visited: boolean;
  radius: number;
  questionsRef: AngularFirestoreCollection<IQuestion>;
  questions: Observable<IQuestion[]>;
}

interface IQuestion {
  correctAnswer: number;
  question: string;
  questionId: string;
  answers: string[];
}

interface IUser {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  placesVisited?: string[]; // places visited with app
  questionsAnswered: number; // questions answered correctly
  level: number;
}

interface ILevel {
  scoreRequirement: number;
}

interface ILevelData {
  current: number;
  next: number | null;
  nextRequired: number | null;
  nextRemaining: number | null;
  currentRequired: number;
}

interface IUserData extends IUser {
  score: number;
  levelData: ILevelData;
}

interface IPlaceMarker extends google.maps.Marker {
  place: IPlace;
}

interface ILatLng {
  lat: number;
  lng: number;
}

interface ILevelItem {
  level: number;
  places: IPlace[];
}
